<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */
class Users
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Article", mappedBy="users")
     */
    private $articles_id;

    public function __construct()
    {
        $this->articles_id = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticlesId(): Collection
    {
        return $this->articles_id;
    }

    public function addArticlesId(Article $articlesId): self
    {
        if (!$this->articles_id->contains($articlesId)) {
            $this->articles_id[] = $articlesId;
            $articlesId->setUsers($this);
        }

        return $this;
    }

    public function removeArticlesId(Article $articlesId): self
    {
        if ($this->articles_id->contains($articlesId)) {
            $this->articles_id->removeElement($articlesId);
            // set the owning side to null (unless already changed)
            if ($articlesId->getUsers() === $this) {
                $articlesId->setUsers(null);
            }
        }

        return $this;
    }
}
