<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends Controller
{

    /**
     * @Route("/")
     */
    public function readAll(){
        $articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
        return $this->render('accueil/index.html.twig',['articles'=>$articles]);
    }
    /**
     * @Route("/article")
    */
    public function index(Request $request){
        $newArticle = new Article();
        $form = $this->createFormBuilder($newArticle)
            ->add('title', TextType::class)
            ->add('content', TextareaType::class)
            ->add('save', SubmitType::class)
            ->getForm();//Récupération du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();
            return $this->redirect('/');
        }
        return $this->render('article/index.html.twig',[
            'controller_name'=>'ArticleController',
            'form'=>$form->createView()
            ]);
    }
    /**
     * @Route("/article/{id}", name="article")
     */
    public function update(Request $request,$id){
        $newArticle = $this->getDoctrine()->getRepository(Article::class)->find($id);
        $form = $this->createFormBuilder($newArticle)
            ->add('title', TextType::class)
            ->add('content', TextareaType::class)
            ->add('save', SubmitType::class)
            ->getForm();//Récupération du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            return $this->redirect('/');
        }
        return $this->render('article/update.html.twig',[
            'update'=>$form->createView()
        ]);
    }

}
